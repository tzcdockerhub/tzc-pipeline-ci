# tzc-pipeline-ci

[DockerHub](https://hub.docker.com/r/thezerocard/tzc-pipeline-ci) | 
[BitBucket](https://bitbucket.org/tzcdockerhub/tzc-pipeline-ci)

Base docker image good for use with CI pipelines. Specifically, its based off the `docker:23.0.5` image, which itself has Docker installed (eg. Docker in Docker). Further, this image installs:

  - docker-compose (https://docs.docker.com/compose/)
  - ecs-deploy (https://github.com/silinternational/ecs-deploy)
  - mysql-client CLI tool
  - other common tooling (bash, wget, curl, unzip, jq, etc). See the Dockerfile for specifics.

---

In CI pipelines (eg. BitBucket), its handy to be able to _execute pre-configured docker-compose_ definitions, but the base images provided by BitBucket miss some common handy tooling. Specifically, this should be used as a generic base image - **not language specific**. To test/deploy project-specific things, you should wire those up as docker-compose files with the requisite language images there.

---

See the provided `example/` directory for a GO project with a BitBucket pipeline configuration (this is not a _working_ example; just demonstrates wiring up a compose project with multiple dependencies, and executing tests).

---

## Updating

This repo is used to build a base image with Docker installed, for use with BitBucket pipelines. Key thing being *the image has Docker installed*, so that it can execute `docker compose` in Bitbucket pipelines. (Eagle eyed readers: this is Docker in Docker).

To build and push an image:
  - pull this repo to your local
  - make changes to the Dockerfile as necessary
  - `git add . && git commit`
  - `git tag <version>` (eg. 1.0.2, or whatever's next)
  - `git push origin <version>`
  - `docker build .`
  - `docker tag <image id> thezerocard/tzc-pipeline-ci:1.0.2`
  - `docker login -u <username> -p <password>` (see lastpass "Docker Hub")
  - `docker push thezerocard/tzc-pipeline-ci:1.0.2`


(probably also want to tag `:latest` and push that as well).

---

In any repos setup for BitBucket pipelines, go and update the `image:` tag at the top of `bitbucket-pipelines.yml`.