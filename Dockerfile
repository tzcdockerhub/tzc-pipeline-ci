# This is a Docker in Docker image, to enable executing docker-compose
# commands *within this container*, with other base tooling installed
# as well.
FROM docker:23.0.5

# Common dependencies
RUN apk --no-cache add \
  curl \
  make \
  openssl \
  wget \
  unzip \
  python3 \
  py3-pip \
  git \
  bash \
  less \
  groff \
  mysql-client \
  gcc \
  pv \
  openssh-client \
  jq && \
  # Install AWS CLI
  pip install awscli httplib2 --upgrade && \
  # Install "ecs-deploy" CLI tool
  curl https://raw.githubusercontent.com/silinternational/ecs-deploy/master/ecs-deploy >> /usr/bin/ecs-deploy && \
    chmod +x /usr/bin/ecs-deploy